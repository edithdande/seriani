<?php

namespace App\Containers\Seriani\UI\API\Transformers;

use App\Containers\Seriani\Models\Seriani;
use App\Ship\Parents\Transformers\Transformer;

class SerianiTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Seriani $entity
     *
     * @return array
     */
    public function transform(Seriani $entity)
    {
        $response = [
            'object' => 'Seriani',
            'id' => $entity->getHashedKey(),
            'email' => $entity->email,
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
