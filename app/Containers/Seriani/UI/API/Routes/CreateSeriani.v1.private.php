<?php

/**
 * @apiGroup           Seriani
 * @apiName            createSeriani
 *
 * @api                {POST} /v1/serianis Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('serianis', [
    'as' => 'api_seriani_create_seriani',
    'uses'  => 'Controller@createSeriani',
    'middleware' => [
      'auth:api',
    ],
]);
