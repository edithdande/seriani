<?php

/**
 * @apiGroup           Seriani
 * @apiName            updateSeriani
 *
 * @api                {PATCH} /v1/serianis/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('serianis/{id}', [
    'as' => 'api_seriani_update_seriani',
    'uses'  => 'Controller@updateSeriani',
    'middleware' => [
      'auth:api',
    ],
]);
