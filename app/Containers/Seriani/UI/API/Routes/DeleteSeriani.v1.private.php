<?php

/**
 * @apiGroup           Seriani
 * @apiName            deleteSeriani
 *
 * @api                {DELETE} /v1/serianis/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('serianis/{id}', [
    'as' => 'api_seriani_delete_seriani',
    'uses'  => 'Controller@deleteSeriani',
    'middleware' => [
      'auth:api',
    ],
]);
