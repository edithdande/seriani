<?php

namespace App\Containers\Seriani\UI\API\Controllers;

use App\Containers\Seriani\UI\API\Requests\CreateSerianiRequest;
use App\Containers\Seriani\UI\API\Requests\DeleteSerianiRequest;
use App\Containers\Seriani\UI\API\Requests\GetAllSerianisRequest;
use App\Containers\Seriani\UI\API\Requests\FindSerianiByIdRequest;
use App\Containers\Seriani\UI\API\Requests\UpdateSerianiRequest;
use App\Containers\Seriani\UI\API\Transformers\SerianiTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Seriani\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateSerianiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSeriani(CreateSerianiRequest $request)
    {
        $seriani = Apiato::call('Seriani@CreateSerianiAction', [$request]);

        return $this->created($this->transform($seriani, SerianiTransformer::class));
    }

    /**
     * @param FindSerianiByIdRequest $request
     * @return array
     */
    public function findSerianiById(FindSerianiByIdRequest $request)
    {
        $seriani = Apiato::call('Seriani@FindSerianiByIdAction', [$request]);

        return $this->transform($seriani, SerianiTransformer::class);
    }

    /**
     * @param GetAllSerianisRequest $request
     * @return array
     */
    public function getAllSerianis(GetAllSerianisRequest $request)
    {
        $serianis = Apiato::call('Seriani@GetAllSerianisAction', [$request]);

        return $this->transform($serianis, SerianiTransformer::class);
    }

    /**
     * @param UpdateSerianiRequest $request
     * @return array
     */
    public function updateSeriani(UpdateSerianiRequest $request)
    {
        $seriani = Apiato::call('Seriani@UpdateSerianiAction', [$request]);

        return $this->transform($seriani, SerianiTransformer::class);
    }

    /**
     * @param DeleteSerianiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSeriani(DeleteSerianiRequest $request)
    {
        Apiato::call('Seriani@DeleteSerianiAction', [$request]);

        return $this->noContent();
    }
}
