<?php

namespace App\Containers\Seriani\UI\WEB\Controllers;

use App\Containers\Seriani\Actions\CreateSerianiAction;
use App\Containers\Seriani\UI\API\Requests\CreateSerianiRequest;
use App\Containers\Seriani\UI\API\Transformers\SerianiTransformer;
use App\Containers\Seriani\UI\WEB\Requests\ContactFormRequest;
use App\Containers\Seriani\UI\WEB\Requests\DeleteSerianiRequest;
use App\Containers\Seriani\UI\WEB\Requests\GetAllSerianisRequest;
use App\Containers\Seriani\UI\WEB\Requests\FindSerianiByIdRequest;
use App\Containers\Seriani\UI\WEB\Requests\UpdateSerianiRequest;
use App\Containers\Seriani\UI\WEB\Requests\StoreSerianiRequest;
use App\Containers\Seriani\UI\WEB\Requests\EditSerianiRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use Mail;

/**
 * Class IndexController
 *
 * @package App\Containers\Seriani\UI\WEB\Controllers
 */
class IndexController extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllSerianisRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
//        $serianis = Apiato::call('Seriani@GetAllSerianisAction', [$request]);
        return view('seriani::index');

        // ..
    }

    /**
     * Show one entity
     *
     * @param FindSerianiByIdRequest $request
     */
    public function show(FindSerianiByIdRequest $request)
    {
        $seriani = Apiato::call('Seriani@FindSerianiByIdAction', [$request]);

        // ..
    }

    /**
     * Add subscriber
     *
     * @param CreateSerianiRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(CreateSerianiRequest $request)
    {
        $this->call(CreateSerianiAction::class, [$request]);
//        return $this->transform($task, TaskTransformer::class);

        return redirect()->route('get_main_home_page')->with('message', 'Subscription successful!');
    }

    /**
     * Add a new entity
     *
     * @param ContactFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContactFormRequest $request)
    {
        $data = array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'message' => $request->get('message')
        );
        Mail::send('seriani::emails.contact', ['data' => $data], function($message) use ($data)
            {
                $message->from($data['email'])
                    ->to('info@serianiasset.com')
                    ->subject($data['subject']);
            });
        return redirect()->route('get_main_home_page')->with('message', 'Thanks for contacting us!');
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditSerianiRequest $request
     */
    public function edit(EditSerianiRequest $request)
    {
        $seriani = Apiato::call('Seriani@GetSerianiByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateSerianiRequest $request
     */
    public function update(UpdateSerianiRequest $request)
    {
        $seriani = Apiato::call('Seriani@UpdateSerianiAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteSerianiRequest $request
     */
    public function delete(DeleteSerianiRequest $request)
    {
         $result = Apiato::call('Seriani@DeleteSerianiAction', [$request]);

         // ..
    }
}
