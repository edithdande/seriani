
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
<p>Dear Admin</p>
<p>
    {{ $data['name'] }} has left a message on {{ url('/') }}:
</p>
<p>
    {{ $data['message'] }}
</p>
</body>
</html>