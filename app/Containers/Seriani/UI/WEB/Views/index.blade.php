@extends('seriani::layouts.default')
@section('content')
    <div class="page-content">
        <div class="header-page">
            <div class="grid-container Header">
                <div class="grid-x" data-equalizer data-equalize-on="medium">
                    <div class="small-12 large-6 cell">
                        <div class="header-content" data-equalizer-watch>
                            <div class="centered">
                                <div class="header-text">
                                    <h4>Introducing Seriani Asset <br>Management</h4>
                                    <h2>Revolutionary customer engagement</h2>
                                    <br>
                                    <br>
                                    <a class="button rounded to-about" href="#about">About the Firm</a>
                                    <div class="arrow bounce">
                                        <a class="fa fa-arrow-down arrow-about" href="#about"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="small-12 medium-6 cell">
                        <div class="header-image" data-equalizer-watch
                             style="background: url(/images/header.svg) right center no-repeat; background-size: contain;">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="large-12 small-12 cell about text-center" id="about">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="large-12 small-12 cell text-center">
                        <h5>ABOUT THE FIRM</h5>
                        {{--<br>--}}
                        {{--<div class="text-center button-group about-buttons">--}}
                            {{--<a class="button secondary clear" href="#">Background</a><a class="button secondary clear" href="#">Phylosophy</a>--}}
                        {{--</div>--}}
                        <br><br>
                        <h3>Who are Seriani Asset Managers?</h3>
                        <br>
                        <p class="about-describe"><strong>SERIANI ASSET MANAGERS LIMITED (SERIANI)</strong> is a limited liability company incorporated
                            in Nairobi, Kenya on 10 th November 2015. It was licensed as a Fund Manager by the Capital
                            Markets Authority (CMA) on 14 th June 2016 to offer investment management and advisory
                            services to clients. <strong>SERIANI ASSET MANAGERS LIMITED (SERIANI)</strong> has an authorized and
                            fully paid up capital of Kshs 40 million.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="large-12 small-12 cell services text-center">
            <div class="grid-container">
                <br>
                <h5>OUR SERVICES</h5>
                <br>
                <div class="grid-x">
                    <div class="small-12 large-8 large-offset-2 cell all-services">
                        <div class="grid-x" data-equalizer>
                            <div class="small-12 large-6 cell" data-equalizer-watch>
                                <div class="text-center blue">
                                    <h5>Estate Planning</h5>
                                    <p>Estate management is a high value service offered as
                                        part of the overall “wealth management”
                                        product offerings. The service is usually offered to ultra-high net worth
                                        individuals, families and
                                        institutions. SERIANI shall have a panel of reputable lawyers to ensure
                                        water-tight estate plans
                                        are developed for clients and the investments are managed at affordable fees.
                                    </p>
                                </div>
                            </div>
                            <div class="small-12 large-6 cell" data-equalizer-watch>
                                <div class="service" style="background-image: url(/images/sample2.jpg)">
                                    <div class="service-description">
                                        <h5>Investment Advisory</h5>
                                        <p>As a licensed fund manager, SERIANI is authorised
                                            to offer investment advisory and corporate
                                            structuring services to clients. In particular, SERIANI is angling to be the
                                            leading institution for
                                            the preparation of Investment Policy Statements (IPS) for registered pension
                                            schemes.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-x" data-equalizer>
                            <div class="small-12 large-5 cell" data-equalizer-watch>
                                <div class="service" style="background-image: url(/images/sample3.jpg)">
                                    <div class="service-description">
                                        <h5>Unit Trusts</h5>
                                        <p>SERIANI will develop differentiated unit trust
                                            products for distribution both to local and foreign
                                            investors. The three types of unit trust products to be offered to investors
                                            include Money Market,
                                            Balanced and Equity Market Funds.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="small-12 large-7 cell text-center" data-equalizer-watch>
                                <div class="text-center white">
                                    <h5>Offshore Products</h5>
                                    <p>
                                        SERIANI will offer its investors an opportunity to invest in products that are
                                        registered
                                        abroad/offshore. These products will mostly come with a wide arena of investment
                                        options and
                                        tax advantages. We are in contact with credible international investment houses
                                        which could
                                        ultimately yield successful partnerships.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="grid-x" data-equalizer>
                            <div class="small-12 large-7 cell" data-equalizer-watch>
                                <div class="service" style="background-image: url(/images/sample5.jpg)">
                                    <div class="service-description">
                                        <h5>Private equity</h5>
                                        <p>
                                            The team at SERIANI has wide experience in the structuring of private equity
                                            transactions and
                                            will aggressively support investors in this line. Key services will be in
                                            deal origination,
                                            structuring, negotiation and execution of private equity deals. In due
                                            course, SERIANI will
                                            develop a Private Equity Fund to mobilise house and third party funds for
                                            onward investment in
                                            private equity opportunities.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="small-12 large-5 cell" data-equalizer-watch>
                                <div class="service" style="background-image: url(/images/sample6.jpg)">
                                    <div class="service-description">
                                    <h5>Portfolio Management</h5>
                                        <p>
                                            This involves a review of potential investment opportunities for clients and
                                            creating an optimal
                                            portfolio for realizing superior returns. SERIANI is set up to provide
                                            flexibility to the clients
                                            regarding their specific short and long term financial objectives.
                                            Management of the portfolio
                                            will largely be discretionary but investors will be kept abreast with the
                                            investment activities on a
                                            regular basis.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <br>
            </div>
        </div>

        <div class="large-12 small-12 cell team">
            <div class="grid-container text-center">
                {{--<div class="text-center button-group team-buttons">--}}
                    {{--<a class="button secondary clear" href="#">Our Board</a><a class="button secondary clear" href="#">Our Team</a>--}}
                {{--</div>--}}
                <div class="grid-x" data-equalizer>
                    <div class="small-12 large-3 cell description" data-equalizer-watch>
                        <h5>Our Team: People behind the amazing Results</h5>
                        <p>
                            Your content stays while design can change in time. When you feel like the time is right now you
                            just change the theme. Your content stays while design can change in time.
                        </p>
                    </div>
                    <div class="small-12 large-9 cell">
                        <div class="slider grid-x grid-padding-x" data-equalizer-watch>
                            <div class="small-4 large-4 cell member">
                                <img src="/images/pic i.jpg" alt="Pic 1">
                            </div>
                            <div class="small-4 large-4 cell member">
                                <img src="/images/pic2.png" alt="Pic 2">
                            </div>
                            <div class="small-4 large-4 cell member">
                                <img src="/images/pic3.png" alt="Pic 3">
                            </div>
                            <div class="small-4 large-4 cell member">
                                <img src="/images/pic i.jpg" alt="Pic 1">
                            </div>
                            <div class="small-4 large-4 cell member">
                                <img src="/images/pic2.png" alt="Pic 2">
                            </div>
                            <div class="small-4 large-4 cell member">
                                <img src="/images/pic3.png" alt="Pic 3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('seriani::partials.footer')
        <div class="reveal" id="contact" data-reveal>
            <br>
            <h4 class="text-center">Talk to us: Our Customer services are here to help you with any queries</h4>
            {{Form::open(['url' => '/store'])}}
            <div class="grid-x grid-margin-x">
                <div class="small-12 large-12 cell">
                    {{ Form::text('name', null, ['placeholder'=> 'Name']) }}
                    {{ Form::text('email', null, ['placeholder' => 'example@emailaddress.com']) }}
                    {{ Form::text('subject', null, ['placeholder' => 'Subject']) }}
                    {{ Form::textarea('message', null, ['placeholder' => 'Type your message here']) }}
                    {{ Form::submit('Submit', ['class' => 'button rounded expanded submit-contact', 'style' => 'background-color: #003A70;
  color: #ffffff']) }}
                </div>
            </div>
            {{Form::close()}}
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endsection