<div class="page-footer">
    <div class="grid-container">
        <div class="message">
            <h2>Reach out to us...</h2>
            <a class="button secondary clear rounded" data-open="contact">Send Message <i class="fa fa-arrow-right"></i></a>
        </div>
        <div class="grid-x contacts">
            {{--<div class="">--}}
                <div class="small-12 large-6 cell left">
                    <div class="grid-x">
                        <div class="large-6 small-12 cell subscribe">
                            <div class="footer-image" style="background: url(/images/Serianifooterlogo.svg) right center no-repeat; background-size: contain;">
                            </div>
                            {{ Form::open(['url' => '/subscribe']) }}
                            <br><br>
                            {{ Form::label('email', 'Want to get updates on Seriani Asset Managers?') }}
                            <br>
                            <div class="input-group large-4 cell text-center">
                                {{ Form::text('email',null,['placeholder'=>'Enter email address', 'class'=>'input-group-field']) }}
                                <div class="input-group-button">
                                    <button class="button secondary" type="submit"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
                <div class="small-12 large-6 cell right">
                    <p>Ojijo Plaza, 3rd Flr, Suite C7, Plums Lane Parklands</p>
                    <p>P. O. Box 14896 - 00800, Nairobi, Kenya</p>
                    <p>0708 758 969, 0780 014 896, 0774 014 896</p>
                </div>
            {{--</div>--}}
        </div>
        <br>
        <p>&copy; <?php echo date("Y"); ?> Seriani Asset Managers.</p>
    </div>
</div>