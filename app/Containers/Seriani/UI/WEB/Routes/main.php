<?php

$router->get('/', [
    'as'   => 'get_main_home_page',
    'uses' => 'IndexController@index',
]);
$router->any('/store', [
    'as'   => 'send_mail',
    'uses' => 'IndexController@store',
]);
$router->post('/subscribe', [
    'as'   => 'subscribe',
    'uses' => 'IndexController@create',
]);
