<?php

namespace App\Containers\Seriani\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateSerianiAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $seriani = Apiato::call('Seriani@UpdateSerianiTask', [$request->id, $data]);

        return $seriani;
    }
}
