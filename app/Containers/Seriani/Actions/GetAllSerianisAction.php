<?php

namespace App\Containers\Seriani\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllSerianisAction extends Action
{
    public function run(Request $request)
    {
        $serianis = Apiato::call('Seriani@GetAllSerianisTask');

        return $serianis;
    }
}
