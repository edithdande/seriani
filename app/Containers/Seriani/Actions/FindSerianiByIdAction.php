<?php

namespace App\Containers\Seriani\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindSerianiByIdAction extends Action
{
    public function run(Request $request)
    {
        $seriani = Apiato::call('Seriani@FindSerianiByIdTask', [$request->id]);

        return $seriani;
    }
}
