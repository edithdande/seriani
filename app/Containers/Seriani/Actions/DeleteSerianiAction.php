<?php

namespace App\Containers\Seriani\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteSerianiAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Seriani@DeleteSerianiTask', [$request->id]);
    }
}
