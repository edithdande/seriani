<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSerianiTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('serianis', function (Blueprint $table) {

            $table->increments('id');
            $table->text('email');
            $table->timestamps();
            //$table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('serianis');
    }
}
