<?php

namespace App\Containers\Seriani\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class SerianiRepository
 */
class SerianiRepository extends Repository
{

    /**
     * The Container Name.
	 * Must be set when the model has a different name than the container
	 */
    protected $container = 'Seriani';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        'email' => 'like'
    ];

}
