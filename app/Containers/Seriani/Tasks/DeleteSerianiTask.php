<?php

namespace App\Containers\Seriani\Tasks;

use App\Containers\Seriani\Data\Repositories\SerianiRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteSerianiTask extends Task
{

    private $repository;

    public function __construct(SerianiRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
