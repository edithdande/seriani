<?php

namespace App\Containers\Seriani\Tasks;

use App\Containers\Seriani\Data\Repositories\SerianiRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateSerianiTask extends Task
{

    private $repository;

    public function __construct(SerianiRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
