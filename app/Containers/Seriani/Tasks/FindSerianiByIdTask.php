<?php

namespace App\Containers\Seriani\Tasks;

use App\Containers\Seriani\Data\Repositories\SerianiRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindSerianiByIdTask extends Task
{

    private $repository;

    public function __construct(SerianiRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
