<?php

namespace App\Containers\Seriani\Tasks;

use App\Containers\Seriani\Data\Repositories\SerianiRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllSerianisTask extends Task
{

    private $repository;

    public function __construct(SerianiRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
