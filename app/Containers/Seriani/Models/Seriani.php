<?php

namespace App\Containers\Seriani\Models;

use App\Ship\Parents\Models\Model;

class Seriani extends Model
{
    protected $fillable = [
        'email'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'serianis';
}
